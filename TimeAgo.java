import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class TimeAgo {

    public static final Map<String, Long> times = new LinkedHashMap<>();

    static {
        times.put("year", TimeUnit.DAYS.toMillis(365));
        times.put("month", TimeUnit.DAYS.toMillis(30));
        times.put("week", TimeUnit.DAYS.toMillis(7));
        times.put("day", TimeUnit.DAYS.toMillis(1));
        times.put("hour", TimeUnit.HOURS.toMillis(1));
        times.put("minute", TimeUnit.MINUTES.toMillis(1));
        times.put("second", TimeUnit.SECONDS.toMillis(1));
    }

    public static String duration(long duration, int maxLevel) {
        StringBuilder res = new StringBuilder();
        int level = 0;
        for (Map.Entry<String, Long> time : times.entrySet()){
            long timeDelta = duration / time.getValue();
            if (timeDelta > 0){
                res.append(timeDelta)
                        .append(" ")
                        .append(time.getKey())
                        .append(timeDelta > 1 ? "s" : "")
                        .append(", ");
                duration -= time.getValue() * timeDelta;
                level++;
            }
            if (level == maxLevel){
                break;
            }
        }
        if ("".equals(res.toString())) {
            return "0 seconds ago";
        } else {
            res.setLength(res.length() - 2);
            res.append(" ago");
            return res.toString();
        }
    }

    public static String duration(long duration) {
        return duration(duration, times.size());
    }

    public static String duration(Date start){
    	Date end = new Date();
        assert start.after(end);
        return duration(end.getTime() - start.getTime(), 1);
    }

    public static String duration(Date start, int level){
    	Date end = new Date();
        assert start.after(end);
        return duration(end.getTime() - start.getTime(), level);
    }

    public static String duration(Date start, Date end){
        assert start.after(end);
        return duration(end.getTime() - start.getTime());
    }

    public static String duration(Date start, Date end, int level){
        assert start.after(end);
        return duration(end.getTime() - start.getTime(), level);
    }

    public static String timeDuration(long seconds, boolean abbreviation) {
        String result = "";
        long milis = seconds * 1000L;

        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, Long> time : times.entrySet()) {
        	long range = milis / time.getValue();
        	if (range > 0L) {
        		String unit = " " + time.getKey();
        		if (abbreviation) {
        			unit = time.getKey().substring(0,1);
        			if ("month".equals(time.getKey())) {
        				unit = unit.toUpperCase();
        			}
        		}
        		sb.append(range);
        		sb.append(unit);
        		sb.append(" ");
        		milis -= range * time.getValue();
        	}
        }
        sb.setLength(sb.length() - 1);

        return sb.toString();
    }

    public static String timeDuration(long seconds) {
    	return timeDuration(seconds, true);
    }

    public static void main(String[] args) {
		Date start = new Date(new Date().getTime() - TimeUnit.DAYS.toMillis(2) - TimeUnit.HOURS.toMillis(11) - TimeUnit.MINUTES.toMillis(8));
		System.out.println(TimeAgo.duration(start));

		long seconds = 686545134L;

		System.out.print(seconds + " seconds = ");
		System.out.println(TimeAgo.timeDuration(seconds));
	}
}